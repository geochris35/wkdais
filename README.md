Website for WKDAIS services

For setup:
ensure pip python and virtualenvwrapper are installed.

at command line:
  mkvirtualenv -p python3 "name_of_your_virtualenv"
  (command "workon" should now list your virtualenv)
  (to enter virtualenv type "workon 'name_of_your_virtualenv'")
  (to exit type deactivate)

  (while in virtualenv)
  pip install -r requirements.txt

To start up server
  ./manage.py runserver
  (you may need to make database as well)

Local address
  localhost:8000
