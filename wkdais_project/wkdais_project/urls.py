"""wkdais_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from client_info import urls as client_urls
from receipt_app import urls as receipt_urls

from .views import redirect_root

urlpatterns = [
    url(r'^$', redirect_root),
    url(r'^receipt/', include(receipt_urls)), # used for printing recipts
    url(r'^', include(client_urls)),
    #url(r'^client/', include(client_urls)),
    url(r'^admin/', admin.site.urls),
]
