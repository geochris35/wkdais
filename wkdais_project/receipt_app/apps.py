from django.apps import AppConfig


class ReceiptAppConfig(AppConfig):
    name = 'receipt_app'
