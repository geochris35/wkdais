from client_info.models import Client
from django.shortcuts import get_object_or_404
from .utils import client_receipt


def some_view(request, slug):
    # receipt_subject = Client.objects.get(slug__iexact='slug')
    receipt_subject = get_object_or_404(
       Client, slug__iexact=slug)
    return client_receipt(receipt_subject)
