from django.http import HttpResponse
from reportlab.pdfgen import canvas
# librarys for different page sizes
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import inch # allows us to measure in inches
from reportlab.lib.colors import black # predefined colors
from datetime import date

# Define constant page width and Height
PAGE_WIDTH = 8.5 * inch
PAGE_HEIGHT = 11 * inch
INFO_LINE_SPACING = .18 * inch
FONT_SIZE = 9.5


def client_receipt(client):
    '''Main function for drawing a pdf of a users total receipt'''

    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=WKDAIS_Receipt.pdf'

    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=letter) # letter pagesize

    # set font for header
    p.setFont('Helvetica', 24, leading=None)
    # Lines above title
    p.setStrokeColor(black)
    p.line(inch*.5,
           PAGE_HEIGHT - (inch*.45),
           PAGE_WIDTH - (inch*.5),
           PAGE_HEIGHT - (inch*.45)
          )
    p.line(inch*.5,
           PAGE_HEIGHT - (inch*.5),
           PAGE_WIDTH - (inch*.5),
           PAGE_HEIGHT - (inch*.5)
          )
    # Draw the title
    p.drawCentredString(PAGE_WIDTH/2,
                        PAGE_HEIGHT - (inch*.85),
                        'Client Information'
                       )

    # Lines below the title
    p.line(inch*.5,
           PAGE_HEIGHT - (inch*.95),
           PAGE_WIDTH - (inch*.45),
           PAGE_HEIGHT - (inch*.95)
          )
    p.line(inch*.5,
           PAGE_HEIGHT - (inch*1),
           PAGE_WIDTH - (inch*.45),
           PAGE_HEIGHT - (inch*1)
          )

    # set font for client info
    p.setFont('Helvetica', 10, leading=None)
    # lines showing client info
    draw_ssn(client, p)
    draw_program(client, p)
    draw_name(client, p)
    draw_intake_date(client, p)
    address_cursor = draw_street_address(client, p)
    draw_completed(client, p)
    draw_city_state_address(client, p, address_cursor)
    draw_dui(client, p)
    draw_paid_in_full(client, p)
    draw_phone(client, p)
    draw_work_phone(client, p)
    draw_is_compliant(client, p)
    draw_birth_date(client, p)
    draw_court(client, p)
    draw_is_inactive(client, p)
    draw_conviction_date(client, p)
    draw_case_number(client, p)
    draw_reactive_date(client, p)
    draw_assessed_date(client, p)
    draw_office_number(client, p)
    draw_bad_address(client, p)

    # line below client info
    p.line(inch*.5,
           PAGE_HEIGHT - (inch*2.6),
           PAGE_WIDTH - (inch*.45),
           PAGE_HEIGHT - (inch*2.6)
          )

    # section for client comments
    draw_info(9, 1, "Client Comments", client, p)
    draw_info(10, 1, "", client.comments, p, colon=False, heading=False)

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()
    return response

def draw_info(row, centering, bold, plain_text,
              canvas, cursor = (0,0,), colon = True,
              heading = True):
    '''Draws to client info section of pdf.  Row numbers are 0 indexed and
    start at top and work down.  Centering starts at 1 for far left, 2 for
    middle, 4 for first quarter and 7 for right quarter.  Bold and plain text
    with be seperated by ': '.  Canvas is the canvas object to be drawn on.'''

    # determine centering
    if centering == 7: # for 7/8 of page
       centering = PAGE_WIDTH/2 + PAGE_WIDTH/4 + (inch*.5)
    elif centering == 1: # for left side of page
        centering = inch*.5
    else:
        centering = PAGE_WIDTH/centering + (inch*.5) # for half, third, and sixth of page

    text_object = canvas.beginText() # begin text object
    text_object.setTextOrigin(centering, #set origin
                              PAGE_HEIGHT - ((inch*1.25) + row*INFO_LINE_SPACING))

    text_object.setFont('Helvetica-Bold', FONT_SIZE, leading=None)
    if (heading == True): # allows user to leave off a heading
        text_object.textOut(str(bold.upper())) # print bold item in all caps
    if cursor != (0,0,): # allows user to not print the cursor and space
        text_object.moveCursor(cursor[0]-36, 0) # BUG - why is it off by 36?
    if colon == True:
        text_object.textOut(': ')
    text_object.setFont('Helvetica', FONT_SIZE, leading=None)
    cursor_loc = text_object.getCursor() # grabs cursor location for other
                                         # lines to use in future
    text_object.textOut(str(plain_text)) # print regular item
    canvas.drawText(text_object)
    return cursor_loc

def draw_ssn(client, canvas):
    '''Draws clients social to pdf.'''

    if(client.ssn):
        draw_info(0, 1, 'SSN', client.ssn, canvas)
    else:
        draw_info(0, 1, 'SSN', '(Missing)', canvas)

def draw_program(client, canvas):
    '''Draws cleints program to pdf.'''

    if(client.program):
        draw_info(0, 2, 'PRogrAM', client.program, canvas)
    else:
        draw_info(0, 2, 'Program', '(Missing)', canvas)

def draw_name(client, canvas):
    '''Draws clients name to pdf.'''

    if (client.first_name and client.last_name):
        full_name = (client.first_name.title() + " " + client.last_name.title())
        draw_info(1, 1, 'NAME', full_name, canvas)
    else:
        draw_info(1, 1, 'NAME', '(Missing)', canvas)

def draw_intake_date(client, canvas):
    '''Prints out proper string for intake date on receipt.  If no intake date
    it prints out "intake date needed". Accepts as arguments
    (client.intake_date, canvas)'''

    if (client.intake_date):
        intake_date = client.intake_date.strftime("%m/%d/%Y")
        draw_info(1, 2, 'Intake Date', intake_date, canvas)
    else:
        draw_info(1, 2, 'Intake Date', '(Missing)', canvas)

def draw_street_address(client, canvas):
    ''' Prints out proper string for stret address.  If no street address
    it prints out "street address needed.  Accepts as aruments
    (client object, canvas)'''

    if (client.street_address):
        cursor_loc = draw_info(2, 1, 'Address', client.street_address, canvas)
    else:
        cursor_loc = draw_info(2, 1, 'Address', '(Missing)', canvas)
    return cursor_loc

def draw_completed(client, canvas):
    '''Prints out proper string to current pdf canvas for wether a client
    has completed their program, and if so, the date they completed it.
    Accepts arguments of (client_object, canvas)'''

    if (client.complete): # If the client has completed their program print yes.
        if (client.completed_date): # print the date of completion
            draw_info(2, 2, 'Complete',
                      'YES ' + client.completed_date.strftime("%m/%d/%Y"), canvas)
        else:
            draw_info(2, 2, 'Complete', 'YES (Missing Date)', canvas)
    else:
        draw_info(2, 2, 'Complete', 'NO', canvas)

def draw_city_state_address(client, canvas, cursor_loc):
    '''Prints out proper string for city, state, and zip of client address.
    If not on file prints out (Missing City), (Missing State), (Missing Zip)'''

    if (client.state and client.city and client.zip_code):
        address = (client.city.title() + ",  " +
                  client.state.upper() + "  " +
                  str(client.zip_code))
        draw_info(3, 1, '', address, canvas, cursor=cursor_loc, colon=False,
                  heading = False)
    else:
        draw_info(3, 1, '', '(Missing)', canvas, cursor=cursor_loc,
                  colon=False, heading = False)


def draw_dui(client, canvas):
    '''Prints which dui client is on to canvas'''

    dui = str(client.dui)
    draw_info(3, 4, 'DUI', dui, canvas)

def draw_paid_in_full(client, canvas):
    '''Prints to canvas if client is paid in full and the date at which
    they were.  If date not know prints (Missing Date).  Accepts arguments
    (client object, canvas)'''

    if (client.paid_in_full_date and client.paid_in_full):
        paid_date = client.paid_in_full_date.strftime("%m/%d/%Y")
        draw_info(3, 2, "Paid in full", 'YES ' + paid_date, canvas)
    elif (client.paid_in_full and not client.paid_in_full_date):
        draw_info(3, 2, "Paid in full", 'YES (Missing Date)', canvas)
    else:
        draw_info(3, 2, "Paid in full", 'NO', canvas)

def draw_phone(client, canvas):
    '''Prints to canvas the clients primary phone number.  If not known prints
    (Missing Phone).  Accepts (client object, canvas)'''

    if (client.phone_number):
        draw_info(4, 1, "Phone", str(client.phone_number), canvas)
    else:
        draw_info(4, 1, "Phone", "(Missing)", canvas)

def draw_work_phone(client, canvas):
    '''Prints to canvas the clients work phone number.  If not known prints
    (Missing Work Phone).  Accepts (client object, canvas)'''

    if (client.work_phone_number):
        draw_info(4, 4, "work Phone", str(client.work_phone_number), canvas)
    else:
        draw_info(4, 4, "work Phone", "(Missing)", canvas)

def draw_is_compliant(client, canvas):
    '''Prints to canvas if client is compliant in training as a "Yes" or
    "No"'''

    if(client.non_compliant == True):
        draw_info(4, 2, "NON-COMPLIANT", "YES", canvas)
    elif(client.non_compliant == False):
        draw_info(4, 2, "NON-COMPLIANT", "NO", canvas)
    else:
        draw_info(4, 2, "NON-COMPLIANT", "(Missing)", canvas)

def draw_birth_date(client, canvas):
    '''Prints to canvas clients birthdate.  If missing, prints "(Missing)"'''

    if (client.birth_date):
        birth_date = str(client.birth_date.strftime("%m/%d/%Y"))
        draw_info(5, 1, "BIRTHDATE", birth_date, canvas)
    else:
        draw_info(5, 1, "BIRTHDATE", "(Missing)", canvas)

def draw_court(client, canvas):
    '''Prints to canvas court client is attending.  If missing, print
    "(Missing)"'''

    if (client.court):
        court = str(client.court)
        draw_info(5, 4, 'Court', court, canvas)
    else:
        draw_info(5, 4, 'Court', '(Missing)', canvas)

def draw_is_inactive(client, canvas):
    '''Prints to canvas wether client is inactive or not.  If missing, prints
    "(Missing)"'''

    if (client.inactive and client.inactive_date):
        inactive_date = client.inactive_date.strftime("%m/%d/%Y")
        draw_info(5, 2, 'INACTIVE', 'YES ' + inactive_date, canvas)
    elif (client.inactive and not client.inactive_date):
        draw_info(5, 2, 'INACTIVE', 'YES (Missing Date)' ,canvas)
    else:
        draw_info(5, 2, 'INACTIVE', 'NO' ,canvas)

def draw_conviction_date(client, canvas):
    '''Prints conviciton date of client.  If missing, prints "(Missing)"'''

    if (client.conviction_date):
        conviction_date = client.conviction_date.strftime("%m/%d/%Y")
        draw_info(6, 1, 'CONVICTION DATE', conviction_date, canvas)
    else:
        draw_info(6, 1, 'CONVICTION DATE', '(Missing)' ,canvas)

def draw_case_number(client, canvas):
    '''Prints case number of client.  If missing, prints "(Missing)"'''

    if (client.case_number):
        case_number = str(client.case_number)
        draw_info(6, 4, 'CASE NUMBER', case_number, canvas)
    else:
        draw_info(6, 4, 'CASE NUMBER', '(Missing)' ,canvas)

def draw_reactive_date(client, canvas):
    '''Prints reactivate date of client.  If missing, prints "(None)"'''

    if (client.reactivate_date):
        reactive_date = client.reactivate_date.strftime("%m/%d/%Y")
        draw_info(6, 2, 'REACTIVE DATE', reactive_date, canvas)
    else:
        draw_info(6, 2, 'REACTIVE DATE', '(None)' ,canvas)

def draw_assessed_date(client, canvas):
    '''Prints assessed date of client.  If missing, prints "(Missing)"'''

    if (client.assessed_date):
        assessed_date = client.assessed_date.strftime("%m/%d/%Y")
        draw_info(7, 1, 'ASSESSED DATE', assessed_date, canvas)
    else:
        draw_info(7, 1, 'ASSESSED DATE', '(Missing)' ,canvas)

def draw_office_number(client, canvas):
    '''Prints office number of client.  If missing, prints "(Missing)"'''

    if (client.office_number):
        office_number = str(client.office_number)
        draw_info(7, 4, 'OFFICE NUMBER', office_number, canvas)
    else:
        draw_info(7, 4, 'OFFICE NUMBER', '(Missing)' ,canvas)

def draw_bad_address(client, canvas):
    '''Print yes if client has a bad address, no if not'''

    if (client.bad_address):
        draw_info(7, 2, 'BAD ADDRESS', 'YES', canvas)
    else:
        draw_info(7, 2, 'BAD ADDRESS', 'NO', canvas)
