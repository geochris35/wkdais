from django.conf.urls import url

from .views import some_view

urlpatterns = [
    url(r'^(?P<slug>[\w\-]+)/$',
        some_view,
        name='client_receipt'),
]
