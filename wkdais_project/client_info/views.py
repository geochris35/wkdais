'''
 Name: Chris Humphreys
 Date: 07/22/17
 Description: Views for Client info app.  Mainly just create, update, list, and
    delete views.
'''

from django.views.generic import TemplateView
from .models import (
    Client, ClientActivities, Payment)
from django.shortcuts import(
    render,redirect, get_object_or_404, get_list_or_404)
from .forms import ClientForm, ActivityForm, PaymentForm
from django.views.generic import View
from django.http import Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy

def client_list(request):
    clients = Client.objects.all()
    return render(
        request,
        'client_info/client_list.html',
        {'client_list': clients})

class ClientCreate(View):
    form_class = ClientForm
    template_name= 'client_info/client_create.html'

    def get(self, request):
        return render(
            request,
            self.template_name,
            {'form': self.form_class(initial = {'slug': ' '})})

    def post(self, request):
        bound_form = self.form_class(request.POST)
        if bound_form.is_valid():
            new_object = bound_form.save()
            new_object.save()
            return redirect(new_object)
        else:
            return render(
                request,
                self.template_name,
                {'form': bound_form})

class ClientDelete(View):
    model = Client
    success_url = reverse_lazy(
        'client_list')
    template_name = (
        'client_info/client_confirm_delete.html')

    def get(self, request, slug):
        obj = get_object_or_404(
            self.model, slug__iexact=slug)
        context = {
            self.model.__name__.lower(): obj,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, slug):
        obj = get_object_or_404(
            self.model, slug__iexact=slug)
        obj.delete()
        return HttpResponseRedirect(
            self.success_url)

def client_detail(request, slug):
    client = get_object_or_404(
       Client, slug__iexact=slug)
    activities = ClientActivities.objects.filter(clients = client)

    return render(
       request,
       'client_info/client.html',
       {'client': client,
        'activities': activities})

class ClientUpdate(View):
    form_class = ClientForm
    model = Client
    template_name = (
        'client_info/client_update_form.html')

    def get(self, request, slug):
        obj = get_object_or_404(
            self.model, slug__iexact=slug)
        context = {
            'form': self.form_class(instance=obj),
            self.model.__name__.lower():obj,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, slug):
        obj = get_object_or_404(
            self.model, slug__iexact=slug)
        bound_form = self.form_class(
            request.POST, instance=obj)
        if bound_form.is_valid():
            new_object = bound_form.save()
            new_object.save()
            return redirect(new_object)
        else:
            context = {
                'form': bound_form,
                self.model.__name__.lower():obj,
            }
            return render(
                request,
                self.template_name,
                context)

def activity_list(request):
    activities = ClientActivities.objects.all()
    return render(
        request,
        'client_info/activity_list.html',
        {'activity_list': activities})

def activity_detail(request, slug):
   activity = get_object_or_404(
       ClientActivities, slug__iexact=slug)

   return render(
       request,
       'client_info/activity_detail.html',
       {'activity': activity})

class ActivityCreate(View):
    form_class = ActivityForm
    template_name= 'client_info/activity_create.html'

    def get(self, request):
        return render(
            request,
            self.template_name,
            {'form': self.form_class(initial = {'slug': ' '})})

    def post(self, request):
        bound_form = self.form_class(request.POST)
        if bound_form.is_valid():
            new_object = bound_form.save()

            # update client paid statuses
            affected_clients = []
            for client in new_object.clients.all():
                client.update_total_billed(new_object.activity_cost)
                client.save()

            new_object.save()

            return redirect(new_object)
        else:
            return render(
                request,
                self.template_name,
                {'form': bound_form})

class ActivityUpdate(View):
    form_class = ActivityForm
    model = ClientActivities
    template_name = (
        'client_info/activity_update_form.html')
    # Used for updating client amount billed.
    old_cost = 0

    def get(self, request, slug):
        obj = get_object_or_404(
            self.model, slug__iexact=slug)
        context = {
            'form': self.form_class(instance=obj),
            self.model.__name__.lower():obj,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, slug):
        obj = get_object_or_404(
            self.model, slug__iexact=slug)
        bound_form = self.form_class(
            request.POST, instance=obj)
        if bound_form.is_valid():
            new_object = bound_form.save()

            # update client profiles with new costs
            for client in obj.clients.all():
                client.update_total_billed(obj.activity_cost*-1)
                client.update_total_billed(new_object.activity_cost)
                client.save()

            new_object.save()
            return redirect(new_object)
        else:
            context = {
                'form': bound_form,
                self.model.__name__.lower():obj,
            }
            return render(
                request,
                self.template_name,
                context)

class ActivityDelete(View):
    model = ClientActivities
    success_url = reverse_lazy(
        'activity_list')
    template_name = (
        'client_info/activity_confirm_delete.html')

    def get(self, request, slug):
        obj = get_object_or_404(
            self.model, slug__iexact=slug)
        context = {
            self.model.__name__.lower(): obj,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, slug):
        obj = get_object_or_404(
            self.model, slug__iexact=slug)

        # remove billed amount from clients profile
        for client in obj.clients.all():
            client.update_total_billed(obj.activity_cost * -1)
            client.save()

        obj.delete()
        return HttpResponseRedirect(
            self.success_url)

def client_payments(request, slug):
    payments = Payment.objects.filter(client__slug=slug)
    client = Client.objects.get(slug__iexact=slug)
    return render(
        request,
        'client_info/client_payments_list.html',
        {'payments': payments, 'client': client})

def payment_detail(request, id):
    payment = get_object_or_404(
       Payment, id__exact=id)

    return render(
       request,
       'client_info/payment_detail.html',
       {'payment': payment})

class PaymentCreate(View):
    form_class = PaymentForm
    template_name= 'client_info/payment_create.html'


    def get(self, request):
        return render(
            request,
            self.template_name,
            {'form': self.form_class})

    def post(self, request):
        '''Saves the form and updates the clients paid in full field.'''
        bound_form = self.form_class(request.POST)
        if bound_form.is_valid():
            new_object = bound_form.save()

            # update client model with new payment
            client = new_object.client
            client.update_total_paid(new_object.payment_amount)
            client.save()

            new_object.save()
            return redirect(new_object)
        else:
            return render(
                request,
                self.template_name,
                {'form': bound_form})

class PaymentDelete(View):
    model = Payment
    success_url = reverse_lazy(
        'client_list')
    template_name = (
        'client_info/payment_confirm_delete.html')

    def get(self, request, id):
        obj = get_object_or_404(
            self.model, id=id)
        context = {
            self.model.__name__.lower(): obj,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, id):
        obj = get_object_or_404(
            self.model, id=id)

        # update client model to remove payment
        client = obj.client
        refund = obj.payment_amount * -1
        client.update_total_paid(refund)
        client.save()

        obj.delete()

        return HttpResponseRedirect(
            self.success_url)

