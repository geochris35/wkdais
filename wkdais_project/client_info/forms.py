from django import forms
from django.forms import ModelForm, SlugField
from django.core.exceptions import ValidationError
from django.utils.text import slugify

from .models import Client, ClientActivities, Payment


class ClientForm(forms.ModelForm):

    slug = forms.SlugField(disabled=True, required=False)

    class Meta:
        model = Client
        fields = '__all__'

    def clean_first_name(self):
        capitalized_name = self.cleaned_data['first_name'].title()
        return capitalized_name

    def clean_last_name(self):
        capitalized_name = self.cleaned_data['last_name'].title()
        return capitalized_name

    def clean_middle_name(self):
        capitalized_name = self.cleaned_data['middle_name'].title()
        return capitalized_name

    def clean_slug(self):
        '''Creates a new slug using a combo of first and last name,
        if client with that first and last name already exists, makes
        a slug that is first_middle_last name.'''

        client_set = Client.objects.all()
        new_slug = (
            self.cleaned_data['first_name'].lower()
            + "_" +
            self.cleaned_data['last_name'].lower()
        )
        for person in client_set:
            if person.slug == new_slug:
                #new_slug += "_" + self.cleaned_data['middle_name'].lower()
                new_slug = (
                    self.cleaned_data['first_name'].lower() + "_" +
                    self.cleaned_data['middle_name'].lower() + "_" +
                    self.cleaned_data['last_name'].lower()
                )
        return slugify(new_slug)

class ActivityForm(forms.ModelForm):

    class Meta:
        model = ClientActivities
        fields = '__all__'

    def clean_slug(self):
        '''Creates a new slug that is a combo of activity type and activity
        number.'''

        new_slug = (self.cleaned_data['type_of_activity'].lower()
                    + "_" +
                    str(self.cleaned_data['activity_ID'])
                   )

        return slugify(new_slug)

class PaymentForm(forms.ModelForm):

    class Meta:
        model = Payment
        fields = '__all__'
