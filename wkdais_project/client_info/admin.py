from django.contrib import admin

# Register your models here.

from .models import (Client, ClientActivities, Payment)

admin.site.register(Client)
admin.site.register(ClientActivities)
admin.site.register(Payment)
