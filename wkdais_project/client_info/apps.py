from django.apps import AppConfig


class ClientInfoConfig(AppConfig):
    name = 'client_info'
