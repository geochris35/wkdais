from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.forms import ModelForm
from django.core.urlresolvers import reverse

# Create your models here.


class Client(models.Model):

    first_name = models.CharField(
        max_length=50)
    last_name = models.CharField(
        max_length=50)
    middle_name = models.CharField(
        max_length=50)
    birth_date = models.DateField(
        null=True)
    conviction_date = models.DateField(
        blank=True,
        null=True)
    case_number = models.CharField(
        max_length = 20,
        blank=True,
        null=True)
    assessed_date = models.DateField(
        blank=True,
        null=True)
    intake_date = models.DateField(
        blank=True,
        null=True)
    balance_cf = models.DecimalField(
        null=True,
        max_digits=7,
        decimal_places=2,
        default='0.00')
    complete = models.NullBooleanField(
        default=True)
    completed_date = models.DateField(
        blank=True,
        null=True)
    paid_in_full = models.NullBooleanField(
        default=False)
    total_paid = models.DecimalField(
        default ='0.00',
        max_digits = 7,
        decimal_places=2)
    total_billed = models.DecimalField(
        default ='0.00',
        max_digits = 7,
        decimal_places=2)
    paid_in_full_date = models.DateField(
        blank=True,
        null=True)
    non_compliant = models.NullBooleanField(
        blank=True,
        null=True)
    non_compliant_date = models.DateField(
        blank=True,
        null=True)
    inactive = models.NullBooleanField(
        blank=True,
        null=True)
    inactive_date = models.DateField(
        blank=True,
        null=True)
    street_address = models.CharField(
        max_length=200,
        blank=True,
        null=True)
    city = models.CharField(
        max_length=75,
        blank=True,
        null=True)
    state = models.CharField(
        max_length=4,
        blank=True,
        null=True)
    zip_code = models.CharField(
        max_length=15,
        blank=True,
        null=True)
    bad_address = models.BooleanField(
        default = False)
    phone_number = models.CharField(
        max_length=15,
        blank=True,
        null=True)
    work_phone_number = models.CharField(
        max_length=15,
        blank=True,
        null=True)
    ssn = models.CharField(
        max_length=15,
        blank=True,
        null=True)
    court = models.CharField(
        max_length=15,
        blank=True,
        null=True)
    office_number = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    dui = models.CharField(
        max_length=5,
        default = '1st')
    comments = models.TextField(
        blank=True,
        null=True,
        help_text='Best Pratice -> DATE: (comment)')
    reactivate_date = models.DateField(
        blank=True,
        null=True)
    program = models.IntegerField(
        blank=True,
        null=True)
    byn = models.CharField(
        max_length=1,
        blank=True,
        null=True)
    slug = models.SlugField(
        max_length=31,
        unique=True,
        help_text='A label for a url config')

    class Meta:
        ordering = ['last_name']
        unique_together = ("first_name",
                           "middle_name",
                           "last_name",
                           "birth_date")
    def __str__(self):
        return self.first_name +' '+ self.last_name

    def get_absolute_url(self):
        return reverse('client_detail',
                       kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('client_delete',
                       kwargs={'slug': self.slug})

    def get_update_url(self):
        return reverse('client_update',
                       kwargs={'slug': self.slug})

    def get_payments_url(self):
        return reverse('client_payments',
                       kwargs={'slug': self.slug})

    def get_receipt_url(self):
        return reverse('client_receipt',
                       kwargs={'slug': self.slug})

    def update_total_paid(self, payment):
        self.total_paid += payment
        self.update_paid_in_full()

    def update_total_billed(self, cost):
        self.total_billed += cost
        self.update_paid_in_full()

    def update_paid_in_full(self):
        if self.total_paid >= self.total_billed:
            self.paid_in_full = True
        else:
            self.paid_in_full = False

class ClientActivities(models.Model):
    activity_ID = models.BigIntegerField(
        unique=True)
    clients = models.ManyToManyField(Client)
    activity_date = models.DateField(
        blank=True,
        null=True)
    activity_cost = models.DecimalField(
        max_digits = 10,
        decimal_places = 2,
        blank=True,
        null=True)
    type_of_activity = models.CharField(
        max_length = 50,
        blank=True)
    notes = models.CharField(
        max_length = 500,
        blank=True)
    slug = models.SlugField(
        max_length = 31,
        unique = True)

    class Meta:
        ordering = ['activity_date']

    def __str__(self):
        return self.type_of_activity + "-" + str(self.activity_ID)

    def get_absolute_url(self):
        return reverse('activity_detail',
                       kwargs={'slug': self.slug})

    def get_update_url(self):
        return reverse('activity_update',
                       kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('activity_delete',
                       kwargs={'slug': self.slug})

class Payment(models.Model):
    ''' Model that represents payments made from client
    to WKDAIS.  Has Fields payment_form, check_number, payment_date,
    payment_amount, and client.  Also has functions get_absolute_url,
    and get_update_url.'''

    PAYMENT_FORMS = (
        ('C', 'Cash'),
        ('K', 'Check'),
        ('V', 'Credit Card'),
    )
    payment_form = models.CharField(max_length=1,
                                    choices=PAYMENT_FORMS)
    check_number = models.IntegerField(
        blank=True,
        null=True,
        )
    payment_date = models.DateField()
    payment_amount = models.DecimalField(
        max_digits=10,
        decimal_places=2)
    client = models.ForeignKey(Client)

    def __str__(self):
        return (str(self.payment_amount) + " "
                + self.client.__str__() + " "
                + str(self.payment_date))

    # this should maybe be in the client model,
    # as it lists the payments for one client.
    # Maybe move later.
    def get_absolute_url(self):
        return reverse('payment_detail',
                       kwargs={'id': self.id})

    def get_delete_url(self):
        return reverse('payment_delete',
                       kwargs={'id': self.id})
