from django.core.management.base import BaseCommand
from client_info.models import Client, ClientActivities, Payment

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def _create_clients(self):
        cChris = Client(first_name = 'Chris',
                        last_name = 'Humphreys',
                        middle_name = 'Charles',
                        slug = 'chris_humphreys',
                        birth_date = '1980-06-18')
        cChris.save()

        cCarrie = Client(first_name = 'Carrie',
                        last_name = 'Humphreys',
                        middle_name = 'Ruth',
                        slug = 'carrie_humphreys',
                        birth_date = '1981-08-23')
        cCarrie.save()

        cBrad = Client(first_name = 'Brad',
                       slug = 'brad_tanner',
                        last_name = 'Tanner',
                        middle_name = 'Wilson',
                        birth_date = '1981-09-15')
        cBrad.save()

    def _create_activities(self):
        cSwimming = ClientActivities(activity_ID = 4862159,
                        activity_cost = 1.00,
                        type_of_activity = 'Swimming',
                        activity_date = '2017-06-18',
                        notes = 'Really Wet',
                        slug = '4862159_swimming')
        cSwimming.save()

        cRunning = ClientActivities(activity_ID = 9832,
                        activity_cost = 3.00,
                        type_of_activity = 'Running',
                        activity_date = '2017-05-08',
                        notes = 'Really Hot',
                        slug = '9832_running')
        cRunning.save()

    def handle(self, *args, **options):
        self._create_clients()
        self._create_activities()

